from distutils.core import setup

setup(
    name='JsonCam',
    version='0.1.0',
    author='Gene Crucean',
    author_email='genecrucean@gmail.com',
    packages=['jsoncam'],
    url='http://pypi.python.org/pypi/JsonCam/',
    license='LICENSE.txt',
    description='Universal camera format and importer / exporter for DCCs.',
    long_description=open('README.txt').read(),
    install_requires=[],
)