# /usr/bin/env Python

"""JsonCam v0.1.0"""

import os
import json
import datetime
import time

import sip
from PyQt4 import uic
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QApplication
from PyQt4.QtGui import QFileDialog

SPEC_VERSION = 1.0

__version__ = 0.1 # This must be compatible with the float type. eg. NOT 0.0.1


class JSONCamUI(QDialog):
    """JSON-Cam user interface class."""

    def __init__(self, uifilepath, parent):
        """Initializes JSONCam UI.

        Arguments:
        uifilepath -- string, File path to the PyQt UI file.
        parent -- PyQt parent object.

        """

        QDialog.__init__(self, parent)
        QApplication.setStyle(QtGui.QStyleFactory.create('Plastique'))
        self.ui = uic.loadUi(uifilepath, self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose) # Ensures that the app is deleted when closed.
        self.setSignals()
        self.init_UI()
        

    def setSignals(self):
        """Connects functions to PyQt signals."""

        self.ui.importButton.released.connect(self.importButtonExecute)
        self.ui.exportButton.released.connect(self.exportButtonExecute)
        self.ui.importBrowseButton.released.connect(self.setFileInLocation)
        self.ui.exportBrowseButton.released.connect(self.setFileOutLocation)
        

    def init_UI(self):
        """Initialize UI items with data from the scene.

        Return:
        True if successful.

        """

        cameras = self.getSceneCameras()
        if not cameras:
            return False

        for eachItem in cameras:
            self.ui.cameraList.addItem(str(eachItem))

        return True
    
    # Signal Functions
    def importButtonExecute(self):
        """Imports the file set in UI.

        Return:
        True if successful.

        """

        fileIn = self.ui.fileInLocationLineEdit.text()
        self.importCamData(fileIn)

        return True


    def exportButtonExecute(self):

        outFile = str(self.ui.fileOutLocationLineEdit.text())
        startFrame = float(self.ui.startFrameLineEdit.text())
        endFrame = float(self.ui.endFrameLineEdit.text())
        step = int(self.ui.stepLineEdit.text())
        camList = self.ui.cameraList.selectedItems()
        department = str(self.ui.departmentLineEdit.text())
        comments = str(self.ui.commentsLineEdit.text())

        self.exportCamData(outFile, startFrame, endFrame, step, camList, department, comments)
        

    def setFileInLocation(self):
        self.fileInLocationLineEdit.setText(self.openFiles())


    def setFileOutLocation(self):
        self.fileOutLocationLineEdit.setText(self.saveFiles())


    
    def importCamData(self, fileIn):
        """Reasd JSONCam file and creates new objects in host application.

        Arguments:
        fileIn -- string, File path to .cam file.

        """

        raise NotImplementedError("JSONCamUI: 'importCamData' method is not implemented in standalone")


    def exportCamData(self, outFile, startFrame, endFrame, step, camList, department=None, comments=None):
        """Serializes camera data into JSON format and writes file on disk.

        Arguments:
        outFile -- string, File path to save data to.
        startFrame -- float, Start frame to use when exporting camera animation.
        endFrame -- float, End frame to use when exporting camera animation.
        step -- int, Interval to use when exporting camera animation.
        camlist -- list, Cameras to export.

        Keyword Arguments:
        department -- ?, ?
        comments -- string, User comments about camera.

        """
        
        raise NotImplementedError("JSONCamUI: 'exportCamData' method is not implemented in standalone.")

       
    def getSceneCameras(self):
        """Get list of cameras in the scene.

        This should be implemented in each DCC sub-class.

        Return:
        List of camera names in the scene.

        """

        return


    def openFiles(self, startDir=None):
        """Open File Dialogue.

        Return: Path to a file.

        """

        options  = QtGui.QFileDialog.DontUseNativeDialog 
        existing = self.fileOutLocationLineEdit.text()

        if len(existing):
            startDir = "//tela/trsam/dome/won/"
        else:
            startDir = existing

        filename = QtGui.QFileDialog.getOpenFileName(self, 'Select file', startDir, 'cam (*.cam)', options=QtGui.QFileDialog.DontUseNativeDialog)

        return filename


    def saveFiles(self, startDir=None):
        """Save File Dialogue.

        Return: Path to a file.

        """

        options  = QtGui.QFileDialog.DontUseNativeDialog 
        existing = self.fileOutLocationLineEdit.text()

        if len(existing):
            startDir = "//tela/trsam/dome/won/"
        else:
            startDir = existing

        filename = QtGui.QFileDialog.getSaveFileName(self, 'Select file', startDir, 'cam (*.cam)', options=QtGui.QFileDialog.DontUseNativeDialog)

        return filename

        
    def showAbout(self):
        """Show About Dialogue."""

        box = QtGui.QMessageBox(self)
        box.setWindowTitle('About JSON Cam')
        box.setText('<html><i>JSON Cam is a small utility to import/export camera data software agnostically.<br/><br/>version: %s<br/>June 26, 2013 <a href="http://www.genecrucean.com" style="color: #A1BDC7">Gene Crucean</a><br/><br/>Python 2.6.6  |   PyQt 4.9.4  |  QtDesigner  |  PyQtForSoftimage <br/></i><html>' % __version__)
        box.setStyleSheet('QPushButton{height: 20px; width: 50px}')
        box.show()


class Exporter(object):
    """JsonCam Exporter."""

    def __init__(self, cameras, frameStart=None, frameEnd=None, frameStep=1):
        """Creates jsoncam exporter and initializes cameras if specified.

        Arguments:
        cameras -- list, Cameras to store in camData attribute.

        Keyword Arguments:
        frameStart -- int, Start frame for storing data.
        frameEnd -- int, End frame for storing data.
        frameStep -- int, Interval to save animation at.

        """

        super(Exporter, self).__init__()

        self.cameras = cameras
        self.frameStart = frameStart
        self.frameEnd = frameEnd
        self.frameStep = frameStep
        self.camData = {
                        "meta":{
                                "comments":None,
                                "createdBy":os.environ["USERNAME"],
                                "dateCreated":datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
                                "unixTime":int(time.time()),
                                "department":None,
                                "software":None,
                                "specVersion": SPEC_VERSION
                               },
                        "cameras":{}
                       }
        self.jsonDump = None

        if self.cameras:
            self.initCamData()


    def initCamData(self):
        """Initializes camData attribute for specified scene cameras.

        Return:
        True if successful.

        """

        if not isinstance(self.cameras, list):
            raise TypeError("JsonCam: 'cameras' argument should be a list.")

        for eachCamera in self.cameras:
            self.camData["cameras"][eachCamera] = {
                                        "anim":None,
                                        "apertureHeight":None,
                                        "apertureWidth":None,
                                        "aspect":None,
                                        "clippingFar":None,
                                        "clippingNear":None,
                                        "focalLength":None,
                                        "pixelRatio":None,
                                        "rotOrder":None,
                                        "frameStart":self.frameStart,
                                        "frameEnd":self.frameEnd,
                                        "frameStep":self.frameStep
                                       }

        self.getCamData()

        return True


    def getCamData(self):
        """Sets data in camData attribute for each camera.

        Should be implemented in sub-class for each DCC.

        Return:
        True if successful.

        """

        # Loop over each camera and store data in its slot in camData dictionary.

        return True


    def export(self, filePath):
        """Export camData to .cam file.

        Arguments:
        filePath -- string, Path to save to.

        Return:
        True if successful.

        """

        if not self.camData:
            raise ValueError("JsonCam: 'camData' attribute is empty!")

        if not os.access(os.path.dirname(filePath), os.W_OK):
            raise IOError("Cannot save to specified directory!")

        with open(filePath, "w") as camFile:
            json.dump(self.camData, camFile, sort_keys=True, indent=4)

        return True