# /usr/bin/env Python

"""JSONCam Softimage Plug-in."""

from win32com.client import constants
from win32com.client import Dispatch
import os
import sys

jsonCamPath = u"S:\DEVELOPMENT\JsonCam"
if jsonCamPath not in sys.path:
    sys.path.append(jsonCamPath)

import jsoncam_si
reload(jsoncam_si)

import sip
from PyQt4 import uic
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QApplication
from PyQt4.QtGui import QFileDialog

si = Application
log = si.LogMessage
collSel = si.Selection


def XSILoadPlugin( in_reg ):
    in_reg.Name = 'JSONCam'
    in_reg.Author = 'Gene Crucean'
    in_reg.Email = 'genecrucean@gmail.com'
    in_reg.Major = 1
    in_reg.Minor = 0
    in_reg.RegisterCommand('JSONCam')
    in_reg.RegisterMenu(constants.siMenuTbRenderPassEditNewPassID, "JSONCam_Menu", False, False)


def JSONCam_Menu_Init( in_ctxt ):
    oMenu = in_ctxt.Source
    oMenu.AddCommandItem("JSONCam","JSONCam")
    return True


def JSONCam_Execute():
    
    uifilepath = XSIUtils.BuildPath(si.Plugins("JSONCam").OriginPath,"..","modules","jsoncam_si","JSONCam.ui")
    
    sianchor = si.getQtSoftimageAnchor()
    sianchor = sip.wrapinstance( long(sianchor), QWidget )
    
    for child in sianchor.children(): # Prevent multiple instances.
        if isinstance(child, jsoncam_si.JSONCamUI):
            return

    dialog = jsoncam_si.JSONCamUI(uifilepath, sianchor)  
    dialog.show()