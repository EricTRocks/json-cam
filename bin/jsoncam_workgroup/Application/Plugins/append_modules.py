# /usr/bin/env Python

"""JSONCam for Softimage appendm modules plugin."""

import sys

def XSILoadPlugin( in_reg ):
    in_reg.Author = "Gene Crucean & Eric Thivierge"
    in_reg.Name = "jsoncam_append_modules"
    in_reg.Email = ""
    in_reg.URL = ""
    in_reg.Major = 1
    in_reg.Minor = 0

    pluginPath = in_reg.OriginPath
    modulesPath = XSIUtils.BuildPath(pluginPath,"..","modules")
        
    if modulesPath not in sys.path:
        sys.path.append(modulesPath)

    return True