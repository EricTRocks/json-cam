# /usr/bin/env Python

"""JsonCam for Softimage"""

from win32com.client import constants
from win32com.client import Dispatch
import os
import json
import datetime
import time
import traceback

import jsoncam
reload(jsoncam)

si = Dispatch("XSI.Application").Application
log = si.LogMessage
sel = si.Selection

SPEC_VERSION = 1.0

__version__ = 0.1 # This must be compatible with the float type. eg. NOT 0.0.1


class JSONCamUI(jsoncam.JSONCamUI):
    """JSONCam UI for Softimage."""

    def __init__(self, uifilepath, parent):
        """Initializes JSONCam UI.

        Arguments:
        uifilepath -- string, File path to the PyQt UI file.
        parent -- PyQt parent object.

        """

        super(JSONCamUI, self).__init__(uifilepath, parent)


    def getSceneCameras(self):

        scnRoot = si.ActiveProject2.ActiveScene.Root
        scnCameras = scnRoot.FindChildren2("", constants.siCameraPrimType, "", True)
        cameras = [x.FullName for x in scnCameras]

        return cameras


    def exportCamData(self, outFile, startFrame, endFrame, step, camList, department=None, comments=None):
        """Serializes camera data into JSON format and writes file on disk.

        Arguments:
        outFile -- string, File path to save data to.
        startFrame -- float, Start frame to use when exporting camera animation.
        endFrame -- float, End frame to use when exporting camera animation.
        step -- int, Interval to use when exporting camera animation.
        camlist -- list, Cameras to export.

        Keyword Arguments:
        department -- ?, ?
        comments -- string, User comments about camera.

        """

        try:
            si.BeginUndo("JSONCamUI Export")

            cameras = [str(x.text()) for x in self.ui.cameraList.selectedItems()]

            exporter = Exporter(cameras, frameStart=startFrame, frameEnd=endFrame, frameStep=step)
            
            if department:
                exporter.camData["meta"]["department"] = department
            if comments:
                exporter.camData["meta"]["comments"] = comments

            log(self.ui.startFrameLineEdit.text().trimmed().isEmpty())

            exporter.export(outFile)

            self.ui.exportOutputMessageLabel.setText("Exported successfully!")

            return True

        except Exception, e:
            tb = traceback.format_exc()
            log(tb)

        finally:
            si.EndUndo()


class Exporter(jsoncam.Exporter):
    """JsonCam Exporter for Softimage."""

    def __init__(self, cameras, frameStart=None, frameEnd=None, frameStep=1):
        """Creates jsoncam exporter and initializes cameras if specified.

        Arguments:
        cameras -- list, Cameras to store in camData attribute.
        frameStart -- int, Start frame for storing data.
        frameEnd -- int, End frame for storing data.
        frameStep -- int, Interval to save animation at.

        """

        super(Exporter, self).__init__(cameras, frameStart=frameStart, frameEnd=frameEnd, frameStep=frameStep)

        self.camData["meta"]["software"] = si.Name + " " + si.Version()
        

    def getCamData(self):
        """Sets data in camData attribute for each camera. Should be implemented in sub-class for each DCC.

        Arguments:
        cameras -- list, Cameras to store in camData attribute.

        """

        playControl = si.ActiveProject2.Properties("Play Control")
        globalIn = playControl.Parameters("GlobalIn").Value
        globalOut = playControl.Parameters("GlobalOut").Value

        for eachItem in self.cameras:

            scnCam = si.Dictionary.GetObject(eachItem, False)
            if not scnCam:
                log(eachItem + " was not found in the scene. Skipping!", 4)
                continue

            # Create collection of params to find animation of.
            animParams = Dispatch("XSI.Collection")
            animParams.Unique = True
            animParams.Add(scnCam.Kinematics.Local.Parameters("posx"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("posy"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("posz"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("rotx"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("roty"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("rotz"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("rotorder"))
            animParams.Add(scnCam.Parameters("aspect"))
            animParams.Add(scnCam.Parameters("pixelratio"))
            animParams.Add(scnCam.Parameters("near"))
            animParams.Add(scnCam.Parameters("far"))
            animParams.Add(scnCam.Kinematics.Local.Parameters("rotorder"))
            animParams.Add(scnCam.Parameters("projplanedist"))
            animParams.Add(scnCam.Parameters("projplanewidth"))
            animParams.Add(scnCam.Parameters("projplaneheight"))

            # Store Camera Data
            camParams = self.camData["cameras"][eachItem] = {}
            camParams["aspect"] = scnCam.Parameters("aspect").Value
            camParams["pixelRatio"] = scnCam.Parameters("pixelratio").Value
            camParams["clippingNear"] = scnCam.Parameters("near").Value
            camParams["clippingFar"] = scnCam.Parameters("far").Value
            camParams["rotOrder"] = scnCam.Parameters("rotorder").Value # 0 = XYZ, 1 = XZY, 2 = YXZ, 3 = YZX, 4 = ZXY, 5 = ZYX

            camParams["focalLength"] = scnCam.Parameters("projplanedist").Value
            camParams["apertureWidth"] = scnCam.Parameters("projplanewidth").Value * 25.4 # Conversion to mm from inches.
            camParams["apertureHeight"] = scnCam.Parameters("projplaneheight").Value * 25.4 # Conversion to mm from inches.

            # camAnimated = si.IsAnimated(scnCam, constants.siFCurveSource, "", "", False)

            # if camAnimated:
            #     camParams["anim"] = {}

            #     if not self.frameStart:
            #         firstFrame = None
            #         for eachItem in animParams:
                        
            #             fcurve = eachItem.Source
            #             if not fcurve:
            #                 continue

            #             fcurve = Dispatch(eachItem.Source)

            #             numKeys = fcurve.Keys.Count
            #             firstKey = fcurve.GetKeyAtIndex(0)
            #             firstKeyFrame = firstKey.Time

            #             if not firstFrame:
            #                 firstFrame = firstKeyFrame
            #             elif firstFrame > firstKeyFrame:
            #                 firstFrame = firstKeyFrame

            #         self.frameStart = firstFrame
            #         camParams["frameStart"] = firstFrame

            #     if not self.frameEnd:
            #         lastFrame = None
            #         for eachItem in animParams:

            #             fcurve = eachItem.Source
            #             if not fcurve:
            #                 continue

            #             fcurve = Dispatch(eachItem.Source)

            #             numKeys = fcurve.Keys.Count
            #             lastKey = fcurve.GetKeyAtIndex(numKeys - 1)
            #             lastKeyFrame = lastKey.Time

            #             if not lastFrame:
            #                 lastFrame = lastKeyFrame
            #             if lastFrame < lastKeyFrame:
            #                 lastFrame = lastKeyFrame

            #         self.frameEnd = lastFrame
            #         camParams["frameEnd"] = lastFrame

            #     for i in xrange(int(self.frameStart), int(self.frameEnd) + 1):
            #         if i%self.frameStep:
            #             continue

            #         frameAnim = camParams["anim"][float(i)] = {}

            #         xform = scnCam.Kinematics.Global.GetTransform2(i)
            #         frameAnim["posx"] = xform.PosX
            #         frameAnim["posy"] = xform.PosY
            #         frameAnim["posz"] = xform.PosZ
            #         frameAnim["rotx"] = xform.RotX
            #         frameAnim["roty"] = xform.RotY
            #         frameAnim["rotz"] = xform.RotZ

            # else:
            camParams["frameStart"] = self.frameStart
            camParams["frameEnd"] = self.frameEnd

        return True

    def export(self, filePath):
        """Export camData to .cam file.

        Arguments:
        filePath -- string, Path to save to.

        Return:
        True if successful.

        """

        super(Exporter, self).export(filePath)
        log("Exported JSONCam to: " + filePath)

        return True