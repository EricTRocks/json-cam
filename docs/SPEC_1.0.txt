SPEC: 1.0

- File contents are 100% json.
- Extension is .cam.
- Script name, plugin, command, tool, otl... whatever it is should be named if possible "JSONCam".
- If possible, try and use the same .ui file in the Softimage dir to maintain consistency. I'm trying to get the entire suite setup using PyQt/PySide if possible.
- It exports key:value (keyframe:value) as an array in posx, posy, posz, rotx, roty, rotz order.
- apertureHeight and apertureWidth are the sensor size in millimeters. (Always store as millimeters in the file. Do conversions on export/import if needed.)
- aspect: duh.
- clippingNear and clippingFar: Clipping planes.
- focalLength: sheesh... c'mon now.
- pixelRatio: Pretty self explanatory.
- rotOrder: 0 = XYZ, 1 = XZY, 2 = YXZ, 3 = YZX, 4 = ZXY, 5 = ZYX. Default is 0.
- startFrame and endFrame is simple the range to bake.
- step will usually be 1 but support key frames other than whole numbers. Half frames for eg.
- metadata: This is a completely open field for each software to dump whatever it wants into.
- specVersion must be saved into file.

Assumptions:

- Camera AOV should always horizontal.
- Cameras are always perspective and not ortho.