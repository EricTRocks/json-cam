"""
APPLICATION:    JSON Cam
VERSION:        0.1
DATE:           June 27, 2013
AUTHOR:         Gene Crucean
DESCRIPTION:    JSON Cam is a tool that exports a json file for easy cross application camera transfer.

NOTES:
Based on SPEC: 1.0

- It exports key:value (keyframe:value) as an array in posx, posy, posz, rotx, roty, rotz order.

- apertureHeight and apertureWidth are the sensor size in inches. (Always store as millimeters in the file. Do conversions on export/import if needed.)
- aspect: duh.
- clippingNear and clippingFar: Clipping planes.
- focalLength: sheesh... c'mon now.
- pixelRatio: Pretty self explanatory.
- rotOrder: 0 = XYZ, 1 = XZY, 2 = YXZ, 3 = YZX, 4 = ZXY, 5 = ZYX. Default is 0.

Assumptions:

- Camera AOV should always horizontal.
- Cameras are always perspective and not ortho.

Thanks:
Eric Thivierge (ethivierge@gmail.com).

"""